import spotify_details

from pypresence import Presence
import os
import time

# rich_presence = Presence(os.environ.get('DISCORD_ID'))
rich_presence = Presence(676202495000969262)


def connect():
    print("Connecting Presence")
    return rich_presence.connect()


def establish_bind(retries=0):
    print("Establishing Bind")
    if retries > 10:
        return
    try:
        connect()
    except:
        print("Couldn't bind with Discord servers")
        time.sleep(1)
        retries += 1
        establish_bind(retries)
    else:
        update_bind()


def update_bind():
    print("Updating Bind")
    start_time = int(time.time())
    print(f"Current time: {start_time}")
    song_start = time.time()
    old_url = ''
    try:
        while True:
            print("Updating Presence")
            url, song, image = spotify_details.get_song('./')
            if url != old_url:
                old_url = url
                song_start = time.time()
            progress, duration, title, popularity, artist, album, image_url = spotify_details.song_details(song)
            print(f'Song Start: {song_start}\nSong End: {song_start + duration}\nSong Duration: {duration}\n')
            print(f'{progress}\n{duration}\n{title}\n{popularity}\n{artist}\n{album}')
            rich_presence.update(state=title, details=artist, start=song_start, end=song_start + duration, small_image='discord')
            time.sleep(5)
    except:
        print("Clearing presence\n")
        rich_presence.clear()
        time.sleep(1)
        update_bind()


if __name__ == '__main__':
    print(os.getenv('DISCORD_ID'))
    try:
        establish_bind()
    except KeyboardInterrupt:
        print("Stopped Rich Presence Script")