import spotipy
from spotipy.oauth2 import SpotifyOAuth
from PIL import Image
import requests

scope = 'user-read-currently-playing'


def get_song(cache_path):
    sp = spotipy.Spotify(auth_manager=SpotifyOAuth(
        scope=scope, cache_path=cache_path + '.cache-light-presence'))
    current_song = sp.currently_playing()
    image_url = current_song['item']['album']['images'][2]['url']
    url, image = download_image(image_url)
    return url, current_song, image


def download_image(url):
    response = requests.get(url, stream=True)
    response.raw.decode_content = True
    image = Image.open(response.raw)
    response.close()
    return url, image


def song_details(song):
    progress = song['progress_ms']
    duration = song['item']['duration_ms']
    title = song['item']['name']
    popularity = song['item']['popularity']
    artist = song['item']['artists'][0]['name']
    album = song['item']['album']['name']
    image_url = song['item']['album']['images'][2]['url']
    return progress / 1000, duration / 1000, title, popularity, artist, album, image_url